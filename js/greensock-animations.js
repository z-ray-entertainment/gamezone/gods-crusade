$(document).ready(function() {
    var overlay_about = document.getElementById("overlay-about");
    var logo = document.getElementsByClassName("logoarea");
    var leftside = document.getElementById("left");
    var rightside = document.getElementById("right");
    TweenMax.from(logo, 1, {y: -100});
    TweenMax.from(leftside, 1, {x: -20});
    TweenMax.from(rightside, 1, {x: 20});
    TweenMax.staggerFrom(".btn", 2, {opacity: 0, ease: Elastic.easeOut, force3D: true}, 0.2);

    //TweenMax.from(overlay_about, 1, {x: 20});
});

$("#start").click(function() {
    TweenMax.staggerTo(".btn", 0.5, {opacity: 0, y: -100, ease: Back.easeIn}, 0.1);
    TweenMax.staggerTo(".logoarea", 0.5, {opacity: 0, y: -100, ease: Back.easeIn}, 0.1);
    setTimeout(function() {
      gotoCharSelect(); // game_init.js
     }, 1000);
});

$("#startfight").click(function() {
    TweenMax.staggerTo(".main", 0.5, {opacity: 0, y: -100, ease: Back.easeIn}, 0.1);
    TweenMax.staggerTo(".logoarea", 0.5, {opacity: 0, y: -100, ease: Back.easeIn}, 0.1);
    setTimeout(function() {
       startGame(); // game_init.js
     }, 1000);
});

$("#p2-toggleAI").click(function() {
    TweenMax.from(this, 0.2, {y: -50});
});


$("#p1-god1").click( function() {
  /*TweenMax.to(god1, 1, {x: -100});*/
  /*
  var p1_current_god = document.getElementById("p1-ChosenGod4");
  TweenMax.staggerTo("#p1-ChosenGod4", 0.5, {opacity: 0, x: -100, ease: Back.easeIn}, 0.1);
  setTimeout(function() {
      $("div#p1-ChosenGod1").show();
      $("div#p1-ChosenGod2").hide();
      $("div#p1-ChosenGod3").hide();
      $("div#p1-ChosenGod4").hide();
      TweenMax.staggerFrom("#p1-ChosenGod1", 0.5, {opacity: 0, x: -100, ease: Back.easeIn}, 0.1);
  }, 1000);
  */
});