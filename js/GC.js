(function () {
    $(function () {
        var ARES = "Ares", LOKI = "Loki", XIPE = "XipeTotec";
        var EVADE = 0, DAMAGE = 1, DEFENCE = 2, HEAL = 3, POISEN = 4, AB_COSTS_MULT = 5,
                AB_EFF_MULT = 6, DEFENCE_PLUS = 7;
        var AB_COSTS = [[], []], AB_EFF = [[], []], AB_DUR = [], AB_DUR_LEFT = [[], []];
        var ANIM_ACTIVE = 0, ANIM_ATTACK = 1, ANIM_HIT = 2, ANIM_SHAKE = 3, ANIM_INFO = 4,
                ANIM_DOGE = 5, ANIM_DEF_PLUS = 6, ANIM_ATK_PLUS = 7, ANIM_FADE_OUT = 8,
                ANIM_SWITCH_PLAYER = 9;

        var animStack = [[], []];
        var playerNames = [], godsNames = [];
        var playerMults = [[], []];
        var playerMultsDefault = [[], []];
        var playerStat = [[], []];
        var playerFP = [], playerAP = [], playerAtk = [], playerAbilities = [[], []];
        var CURRENT_PLAYER = 0;
        var burnFP = [], evadeIncrease = [], maxFP = [], maxAP = [];
        function newGame() {
            randomSpecialAbility(0, 0);
            randomSpecialAbility(0, 1);
            randomSpecialAbility(0, 2);
            randomSpecialAbility(1, 0);
            randomSpecialAbility(1, 1);
            randomSpecialAbility(1, 2);
            playerNames[0] = getParameterByUrl("p1_name");
            playerNames[1] = getParameterByUrl("p2_name");
            var char1 = getParameterByUrl("p1_god");
            var char2 = getParameterByUrl("p2_god");
            initChars([char1, char2]);
            $("#infoSpan").text("Starte Neues Spiel");
            castAnimation(-1, ANIM_ACTIVE, false);
            refreshUI();
        }

        function initChars(chars) {
            for (var i = 0; i < playerNames.length; i++) {
                initChar(chars[i], i);
            }
        }

        function initChar(char, player) {
            switch (char) {
                case ARES :
                    playerFP[player] = 400;
                    maxFP[player] = 400;
                    playerAP[player] = 125;
                    maxAP[player] = 125;
                    playerAtk[player] = 25;
                    playerMults[player][DAMAGE] = 1.2;
                    playerMults[player][DEFENCE] = 1.2;
                    playerMults[player][EVADE] = 0;
                    playerMults[player][AB_COSTS_MULT] = 1;
                    playerMults[player][AB_EFF_MULT] = 0.6;
                    playerMultsDefault[player][DAMAGE] = 1.2;
                    playerMultsDefault[player][DEFENCE] = 1.2;
                    playerMultsDefault[player][EVADE] = 0;
                    playerMultsDefault[player][AB_COSTS_MULT] = 0.5;
                    playerMultsDefault[player][AB_EFF_MULT] = 0.8;
                    godsNames[player] = ARES;
                    AB_COSTS[player][HEAL] = 50;
                    AB_COSTS[player][POISEN] = 20;
                    AB_COSTS[player][DEFENCE] = 10;
                    AB_COSTS[player][DEFENCE_PLUS] = 10;
                    AB_COSTS[player][DAMAGE] = 10;
                    AB_COSTS[player][EVADE] = 10;
                    AB_EFF[player][HEAL] = 50;
                    AB_EFF[player][POISEN] = 20;
                    AB_EFF[player][DEFENCE] = 4;
                    AB_EFF[player][DAMAGE] = 4;
                    AB_EFF[player][EVADE] = 0;
                    evadeIncrease[player] = 0.1;
                    break;
                case LOKI :
                    playerFP[player] = 125;
                    maxFP[player] = 125;
                    playerAP[player] = 500;
                    maxAP[player] = 500;
                    playerAtk[player] = 20;
                    playerMults[player][DAMAGE] = 0.75;
                    playerMults[player][DEFENCE] = 1.5;
                    playerMults[player][EVADE] = 0.1;
                    playerMults[player][AB_COSTS_MULT] = 1;
                    playerMults[player][AB_EFF_MULT] = 1;
                    playerMultsDefault[player][DAMAGE] = 0.75;
                    playerMultsDefault[player][DEFENCE] = 1.5;
                    playerMultsDefault[player][EVADE] = 0.3;
                    playerMultsDefault[player][AB_COSTS_MULT] = 1;
                    playerMultsDefault[player][AB_EFF_MULT] = 1;
                    godsNames[player] = LOKI;
                    AB_COSTS[player][HEAL] = 50;
                    AB_COSTS[player][POISEN] = 20;
                    AB_COSTS[player][DEFENCE] = 10;
                    AB_COSTS[player][DEFENCE_PLUS] = 10;
                    AB_COSTS[player][DAMAGE] = 10;
                    AB_COSTS[player][EVADE] = 10;
                    AB_EFF[player][HEAL] = 50;
                    AB_EFF[player][POISEN] = 20;
                    AB_EFF[player][DEFENCE] = 2;
                    AB_EFF[player][DAMAGE] = 2;
                    AB_EFF[player][EVADE] = 0.5;
                    evadeIncrease[player] = 0.1;
                    break;
                case XIPE :
                    playerFP[player] = 250;
                    maxFP[player] = 250;
                    playerAP[player] = 250;
                    maxAP[player] = 250;
                    playerAtk[player] = 20;
                    playerMults[player][DAMAGE] = 1;
                    playerMults[player][DEFENCE] = 1;
                    playerMults[player][EVADE] = 0.3;
                    playerMults[player][AB_COSTS_MULT] = 0.5;
                    playerMults[player][AB_EFF_MULT] = 3;
                    playerMultsDefault[player][DAMAGE] = 1;
                    playerMultsDefault[player][DEFENCE] = 1;
                    playerMultsDefault[player][EVADE] = 0.15;
                    playerMultsDefault[player][AB_COSTS_MULT] = 0.5;
                    playerMultsDefault[player][AB_EFF_MULT] = 3;
                    godsNames[player] = XIPE;
                    AB_COSTS[player][HEAL] = 50;
                    AB_COSTS[player][POISEN] = 20;
                    AB_COSTS[player][DEFENCE] = 10;
                    AB_COSTS[player][DEFENCE_PLUS] = 10;
                    AB_COSTS[player][DAMAGE] = 10;
                    AB_COSTS[player][EVADE] = 10;
                    AB_EFF[player][HEAL] = 50;
                    AB_EFF[player][POISEN] = 20;
                    AB_EFF[player][DEFENCE] = 2;
                    AB_EFF[player][DAMAGE] = 2;
                    AB_EFF[player][EVADE] = 0.2;
                    evadeIncrease[player] = 0.1;
                    break;
            }
            burnFP[player] = (playerFP[player] / 100) * 30;
        }

        function randomSpecialAbility(player, slot) {
            var rand = random(5);
            switch (rand) {
                case 0 :
                    playerAbilities[player][slot] = HEAL;
                    break;
                case 1 :
                    playerAbilities[player][slot] = POISEN;
                    break;
                case 2 :
                    playerAbilities[player][slot] = DEFENCE_PLUS;
                    break;
                case 3 :
                    playerAbilities[player][slot] = DAMAGE;
                    break;
                case 4 :
                    playerAbilities[player][slot] = EVADE;
                    break;
                default :
                    $("#infoSpan").text(playerAbilities[player][slot]);
                    break;
            }
        }

        function switchPlayer() {
            stopAnimaton(-1, ANIM_ACTIVE, false);
            checkWin();
            CURRENT_PLAYER++;
            CURRENT_PLAYER %= 2;
            checkStats();
            castAnimation(-1, ANIM_ACTIVE, false);
            refreshUI();
            if (CURRENT_PLAYER === 1 && getParameterByUrl("isPlayer2KI") === "true") {
                doAI();
            }
        }

        function checkStats() {
            for (var i = 0; i < 2; i++) {
                for (var j = 0; j < playerStat[i].length; j++) {
                    switch (playerStat[i][j]) {
                        case POISEN :
                            var dmg = getIncrease(POISEN, (i + 1) % 2);
                            playerFP[CURRENT_PLAYER] -= dmg;
                            displayInfo((i + 1) % 2, "Vergiftet: " + getIncrease(POISEN, (i + 1) % 2), false);
                            AB_DUR_LEFT[i][POISEN]--;
                            $("#infoSpan").text("Poisen Left: " + AB_DUR_LEFT[i][POISEN]);
                            if (AB_DUR_LEFT[i][POISEN] <= 0) {
                                removeStat(POISEN, CURRENT_PLAYER);
                            }
                            break;
                        case EVADE :

                            break;
                    }
                }
            }
            checkWin();
        }

        function doAI() {
            $("#infoSpan").text("Did AI");
            var action = random(6);
            switch (action) {
                case 0 :
                default :
                    attack(1);
                    switchPlayer();
                    break;
                case 1 :
                    AB_DUR_LEFT[CURRENT_PLAYER][DEFENCE] = 1;
                    addStat(DEFENCE, CURRENT_PLAYER);
                    switchPlayer();
                    break;
                case 2 :
                    if (useAbility(playerAbilities[CURRENT_PLAYER][0])) {
                        randomSpecialAbility(CURRENT_PLAYER, 0);
                        switchPlayer();
                    } else {
                        doAI();
                    }
                    break;
                case 3 :
                    if (useAbility(playerAbilities[CURRENT_PLAYER][1])) {
                        randomSpecialAbility(CURRENT_PLAYER, 1);
                        switchPlayer();
                    } else {
                        doAI();
                    }
                    break;
                case 4 :
                    if (useAbility(playerAbilities[CURRENT_PLAYER][2])) {
                        randomSpecialAbility(CURRENT_PLAYER, 2);
                        switchPlayer();
                    } else {
                        doAI();
                    }
                    break;
                case 5 :
                    switch (godsNames[CURRENT_PLAYER]) {
                        case ARES :
                            useAbility(DAMAGE);
                            break;
                        case LOKI :
                            useAbility(EVADE);
                            break;
                        case XIPE :
                            useAbility(HEAL);
                            break;
                    }
                    switchPlayer();
                    break;
            }
        }

        function attack(multiPlikator) {
            castAnimation(-1, ANIM_ATTACK, true);
            var target = (CURRENT_PLAYER + 1) % 2;
            var evadeChace = getIncrease(EVADE, target);
            $("#infoSpan").text("Evade: " + evadeChace);
            if (Math.random() > evadeChace) {
                castAnimation(-1, ANIM_HIT, true);
                castAnimation(-1, ANIM_SHAKE, true);
                var dmg = ((playerAtk[CURRENT_PLAYER] * playerMults[CURRENT_PLAYER][DAMAGE]) / playerMults[target][DEFENCE]) * multiPlikator;
                dmg = Math.round(dmg);
                playerFP[target] -= dmg;
                displayInfo(target, dmg + " Schaden", false);
            } else {
                displayInfo(target, "Verfehlt!", false);
                castAnimation(-1, ANIM_DOGE, true);
            }
            refreshUI();
        }

        function displayInfo(player, msg, good) {
            var lableID = "";
            switch (player) {
                case 0 :
                    lableID = "#info-left";
                    break;
                case 1 :
                    lableID = "#info-right";
                    break;
            }
            if (good) {
                $(lableID).css({color: "#0f0"});
            } else {
                $(lableID).css({color: "#f00"});
            }
            $(lableID).text(msg);
            castAnimation(lableID, ANIM_INFO, true);
        }

        function random(range) {
            return Math.floor(Math.random() * range);
        }

        function getIncrease(ability, player) {
            switch (ability) {
                case HEAL :
                    return  AB_EFF[player][HEAL] * playerMults[player][AB_EFF_MULT];
                case POISEN:
                    return  AB_EFF[(player + 1) % 2][POISEN] * playerMults[(player + 1) % 2][AB_EFF_MULT];
                case DEFENCE :
                    return playerMultsDefault[player][DEFENCE] * AB_EFF[player][DEFENCE] * playerMults[player][AB_EFF_MULT];
                case DEFENCE_PLUS :
                    return playerMultsDefault[player][DEFENCE] * AB_EFF[player][DEFENCE] * playerMults[player][AB_EFF_MULT] * 2;
                case DAMAGE :
                    return AB_EFF[player][DAMAGE] * playerMults[player][AB_EFF_MULT];
                case EVADE :
                    return  (AB_EFF[player][EVADE] * playerMults[player][AB_EFF_MULT]).toFixed(1);
            }
            return 0;
        }

        function getABCost(player, ability) {
            return AB_COSTS[player][ability] / playerMults[player][AB_COSTS_MULT];
        }

        function useAbility(ability) {
            var costs = getABCost(CURRENT_PLAYER, ability);
            if (costs <= playerAP[CURRENT_PLAYER]) {
                playerAP[CURRENT_PLAYER] -= getABCost(CURRENT_PLAYER, ability);
                switch (ability) {
                    case DAMAGE :
                        displayInfo(CURRENT_PLAYER, "Greife 2mal an", true);
                        attack(2);
                        break;
                    case HEAL :
                        displayInfo(CURRENT_PLAYER, "Heile: " + getIncrease(HEAL, CURRENT_PLAYER), true);
                        var maxFP = 0;
                        switch (godsNames[CURRENT_PLAYER]) {
                            case ARES :
                                maxFP = 500;
                                break;
                            case LOKI :
                                maxFP = 125;
                                break;
                            case XIPE :
                                maxFP = 250;
                                break;
                        }
                        playerFP[CURRENT_PLAYER] += getIncrease(HEAL, CURRENT_PLAYER);
                        if (playerFP[CURRENT_PLAYER] > maxFP) {
                            playerFP[CURRENT_PLAYER] = maxFP;
                        }
                        break;
                    case DEFENCE_PLUS :
                        displayInfo(CURRENT_PLAYER, "Erhoehe Vert.: " + getIncrease(DEFENCE_PLUS, CURRENT_PLAYER), true);
                        playerMults[CURRENT_PLAYER][DEFENCE] = getIncrease(DEFENCE_PLUS, CURRENT_PLAYER);
                        addStat(DEFENCE_PLUS, CURRENT_PLAYER);
                        castAnimation(-1, ANIM_DEF_PLUS, true);
                        break;
                    case EVADE :
                        displayInfo(CURRENT_PLAYER, "Erhoehe Agilitaet: " + getIncrease(EVADE, CURRENT_PLAYER), true);
                        playerMults[CURRENT_PLAYER][EVADE] = getIncrease(EVADE, CURRENT_PLAYER);
                        addStat(EVADE, CURRENT_PLAYER);
                        switch (CURRENT_PLAYER) {
                            case 0 :
                                castAnimation("#left", ANIM_DOGE, true);
                                break;
                            case 1 :
                                castAnimation("#right", ANIM_DOGE, true);
                                break;
                        }
                        break;
                    case POISEN :
                        var target = (CURRENT_PLAYER + 1) % 2;
                        displayInfo(CURRENT_PLAYER, "Vergifte Gegner " + getIncrease(POISEN, CURRENT_PLAYER), true);
                        addStat(POISEN, target);
                        break;
                }
                return true;
            } else {
                displayInfo(CURRENT_PLAYER, "Nicht genug Faehigkeit " + costs + " noetig", false);
                return false;
            }
            refreshUI();
        }

        function removeStat(stat, player) {
            for (var i = 0; i < playerStat[player].length; i++) {
                if (playerStat[player][i] === stat) {
                    playerStat[player][i] = -1;
                }
            }
        }

        function addStat(stat, player) {
            if (AB_DUR_LEFT === undefined) {
                AB_DUR_LEFT = new Array(1);
            }
            if (AB_DUR_LEFT[player] === undefined) {
                AB_DUR_LEFT[player] = new Array(1);
            }
            AB_DUR_LEFT[player][stat] = AB_DUR[stat];
            $("#infoSpan").text("Diration Left: " + AB_DUR_LEFT[player][stat] + " " + AB_DUR[stat]);
            for (var i = 0; i < playerStat[player].length; i++) {
                if (playerStat[player][i] === -1) {
                    playerStat[player][i] = stat;
                    return;
                } else if (playerStat[player][i] === stat) {
                    return;
                }
            }
            var newSlot = playerStat[CURRENT_PLAYER].length;
            playerStat[CURRENT_PLAYER][newSlot] = stat;
        }

        function refreshUI() {
            var rightPercentFP = 100/maxFP[1]*playerFP[1];
            var leftPercentFP = 100/maxFP[0]*playerFP[0];
            if (playerFP[0] < 0) {
                $('#faithP1').text("Glauben: 0");
            } else {
                $('#faithP1').text("Glauben: " + playerFP[0]);
            }
            if (playerFP[1] < 0) {
                $('#faithP2').text("Glauben: 0");
            } else {
                $('#faithP2').text("Glauben: " + playerFP[1]);
            }
            $("#fpLeft").css({"background-repeat" : "no-repeat", "background-size" : leftPercentFP+"% 100%"});
            $("#fpRight").css({"background-repeat" : "no-repeat", "background-size" : rightPercentFP+"% 100%"});
            var rightPercent = 100/maxAP[1]*playerAP[1];
            var leftPercent = 100/maxAP[0]*playerAP[0];
            $("#apRight").css({"background-repeat" : "no-repeat", "background-size" : rightPercent+"% 100%"});
            $("#apLeft").css({"background-repeat" : "no-repeat", "background-size" : leftPercent+"% 100%"});
            $("#abilityP1").text("Faehigkeit: " + playerAP[0]);
            $("#abilityP2").text("Faehigkeit: " + playerAP[1]);
            switch(CURRENT_PLAYER){
                case 0 :
                    $('#curPlayer').css({color:"#a00"});
                    break;
                case 1 :
                    $('#curPlayer').css({color:"#00a"});
                    break;
            }
            $('#curPlayer').text("Spieler " + (CURRENT_PLAYER + 1) + ": " + playerNames[CURRENT_PLAYER]);
            for (var i = 0; i < 2; i++) {
                var imgID = "";
                switch (i) {
                    case 0 :
                        imgID = "#city-left";
                        break;
                    case 1 :
                        imgID = "#city-right";
                        break;
                }
                if (playerFP[i] <= burnFP[i]) {
                    switch (godsNames[i]) {
                        case ARES :
                            $(imgID).prop('src', 'img/city_Ares_destro.png');
                            break;
                        case LOKI :
                            $(imgID).prop('src', 'img/city_Loki_destro.png');
                            break;
                        case XIPE :
                            $(imgID).prop('src', 'img/city_XipeTotec_destro.png');
                            break;
                    }
                } else {
                    switch (godsNames[i]) {
                        case ARES :
                            $(imgID).prop('src', 'img/city_Ares.png');
                            break;
                        case LOKI :
                            $(imgID).prop('src', 'img/city_Loki.png');
                            break;
                        case XIPE :
                            $(imgID).prop('src', 'img/city_XipeTotec.png');
                            break;
                    }
                }
            }

            switch (godsNames[CURRENT_PLAYER]) {
                case ARES :
                    $("#btMagic").text("2x Angreiffen " + getIncrease(DAMAGE, CURRENT_PLAYER) + "/" + getABCost(CURRENT_PLAYER, DAMAGE));
                    break;
                case LOKI :
                    $("#btMagic").text("Ausweichen " + getIncrease(EVADE, CURRENT_PLAYER) + "/" + getABCost(CURRENT_PLAYER, EVADE));
                    break;
                case XIPE :
                    $("#btMagic").text("Heilen " + getIncrease(HEAL, CURRENT_PLAYER) + "/" + getABCost(CURRENT_PLAYER, HEAL));
                    ;
                    break;
            }

            for (var j = 0; j < 3; j++) {
                var btID = "#imgAB" + (j + 1);
                var lable = "#btAB" + (j + 1) + "Label";
                var ability = playerAbilities[CURRENT_PLAYER][j];
                switch (ability) {
                    case DEFENCE_PLUS :
                        $(btID).prop('src', 'img/button_def2x.png');
                        $(lable).text("Doppelte Vert.: " + getIncrease(ability, CURRENT_PLAYER) + "/" + getABCost(CURRENT_PLAYER, ability));
                        break;
                    case DAMAGE :
                        $(btID).prop('src', 'img/button_atk2x.png');
                        $(lable).text("Doppel Schlag: " + getIncrease(ability, CURRENT_PLAYER) + "/" + getABCost(CURRENT_PLAYER, ability));
                        break;
                    case HEAL :
                        $(btID).prop('src', 'img/button_heal.png');
                        $(lable).text("Heile " + getIncrease(ability, CURRENT_PLAYER) + "/" + getABCost(CURRENT_PLAYER, ability));
                        break;
                    case POISEN :
                        $(btID).prop('src', 'img/button_poison.png');
                        $(lable).text("Vergiften: " + getIncrease(ability, CURRENT_PLAYER) + "/" + getABCost(CURRENT_PLAYER, ability));
                        break;
                    case EVADE :
                        $(btID).prop('src', 'img/button_dodge.png');
                        $(lable).text("Agilitaet: " + getIncrease(ability, CURRENT_PLAYER) + "/" + getABCost(CURRENT_PLAYER, ability));
                        break;
                    default :
                        //$("#infoSpan").text("RefreshUI: Error mit Ability: " +playerAbilities[CURRENT_PLAYER][j] + " @" + btID+" CURRENT_PLAYER: "+CURRENT_PLAYER+" j:"+j);
                        break;
                }
            }
            $("#godNameP1").text(godsNames[0]);
            $("#godNameP2").text(godsNames[1]);
        }

        function checkWin() {
            if (playerFP[((CURRENT_PLAYER + 1) % 2)] <= 0) {
                if(getParameterByUrl("isPlayer2KI") === "true" && CURRENT_PLAYER === 1){
                    openNav(7);
                }
                else{
                    openNav(6);
                    $("#winnerName").text(playerNames[CURRENT_PLAYER]);
                }
                switch ((CURRENT_PLAYER + 1) % 2) {
                    case 0 :
                        castAnimation("#left", ANIM_FADE_OUT, true);
                        castAnimation("#city-left", ANIM_FADE_OUT, true);
                        break;
                    case 1 :
                        castAnimation("#right", ANIM_FADE_OUT, true);
                        castAnimation("#city-right", ANIM_FADE_OUT, true);
                        break;
                }

            }
        }

        function castAnimation(id, anim, force) {
            var fac = 1;
            if (id === -1) {
                switch (anim) {
                    case ANIM_ACTIVE :
                    case ANIM_ATTACK :
                    case ANIM_DEF_PLUS :
                        switch (CURRENT_PLAYER) {
                            case 0 :
                                id = "#left";
                                break;
                            case 1 :
                                id = "#right";
                                fac = -1;
                                break;
                        }
                        break;
                    case ANIM_DOGE :
                        switch (CURRENT_PLAYER) {
                            case 0 :
                                id = "#right";
                                fac = -1;
                                break;
                            case 1 :
                                id = "#left";
                                break;
                        }
                        break;
                    case ANIM_HIT :
                        switch (CURRENT_PLAYER) {
                            case 1 :
                                id = "#left";
                                break;
                            case 0 :
                                id = "#right";
                                fac = -1;
                                break;
                        }
                        break;
                    case ANIM_SHAKE :
                        switch (CURRENT_PLAYER) {
                            case 1 :
                                id = "#city-left";
                                fac = -1;
                                break;
                            case 0 :
                                id = "#city-right";
                                break;
                        }
                        break;
                    case ANIM_FADE_OUT :
                        break;
                    case ANIM_SWITCH_PLAYER :
                        id="#curPlayerInfo";
                        switch (CURRENT_PLAYER){
                            case 0 :
                                fac = -1;
                                break;
                        }
                        break;
                }
            }
            if (animStack[anim] === undefined) {
                animStack[anim] = new Array(1);
            }
            if (animStack[anim][id] === undefined || force) {
                switch (anim) {
                    case ANIM_ACTIVE :
                        animStack[anim][id] = new TimelineMax({repeat: -1});
                        animStack[anim][id].to(id, 2, {y: "20px", ease: Quad.easeInOut})
                                .to(id, 2, {y: "0px", ease: Quad.easeInOut});
                        break;
                    case ANIM_ATTACK :
                        animStack[anim][id] = new TimelineMax();
                        animStack[anim][id].to(id, 0.2, {x: (20 * fac) + "px", ease: Quad.easeInOut})
                                .to(id, 0.2, {x: "0px", ease: Quad.easeInOut});
                        break;
                    case ANIM_HIT :
                        animStack[anim][id] = new TimelineMax({delay: 0.2});
                        switch (fac) {
                            case -1 :
                                animStack[anim][id].to(id, 0.2, {rotation: (180 + 10), ease: Quad.easeInOut})
                                        .to(id, 0.2, {rotation: "180", ease: Quad.easeInOut});
                                break;
                            case 1 :
                                animStack[anim][id].to(id, 0.2, {rotation: (-10), ease: Quad.easeInOut})
                                        .to(id, 0.2, {rotation: "0", ease: Quad.easeInOut});
                                break;
                        }
                        break;
                    case ANIM_SHAKE :
                        animStack[anim][id] = new TimelineMax({repeat: 10});
                        animStack[anim][id].to(id, 0.025, {x: (5 * fac) + "px", ease: Linear.easeInOut})
                                .to(id, 0.05, {x: "0px", ease: Linear.easeInOut});
                        break;
                    case ANIM_INFO :
                        animStack[anim][id] = new TimelineMax();
                        animStack[anim][id].to(id, 1, {y: "-5px", opacity: 1, ease: Linear.easeInOut})
                                .to(id, 1, {y: "-10px", opacity: 0, ease: Linear.easeInOut})
                                .to(id, 1, {y: "0px", opacity: 0, ease: Linear.easeInOut});
                        break;
                    case ANIM_DOGE :
                        animStack[anim][id] = new TimelineMax();
                        animStack[anim][id].to(id, 0.2, {x: (-20 * fac) + "px", ease: Quad.easeInOut})
                                .to(id, 0.2, {x: "0px", ease: Quad.easeInOut});
                        break;
                    case ANIM_DEF_PLUS :
                        animStack[anim][id] = new TimelineMax();
                        animStack[anim][id].to(id, 0.2, {scaleX: 0.8, scaleY: 0.8 * fac, ease: Quad.easeInOut})
                                .to(id, 0.2, {scaleX: 1, scaleY: 1 * fac, ease: Quad.easeInOut});
                        break;
                    case ANIM_FADE_OUT :
                        animStack[anim][id] = new TimelineMax();
                        animStack[anim][id].to(id, 2, {opacity: 0, ease: Linear.easeInOut});
                        break;
                    case ANIM_SWITCH_PLAYER :
                        animStack[anim][id] = new TimelineMax();
                        switch (fac){
                            case 1 :
                                animStack[anim][id].to(id, 1, {x: "50px", ease: Linear.easeInOut});
                                break;
                            case -1 :
                                animStack[anim][id].to(id, 1, {x: "-50px", ease: Linear.easeInOut});
                                break;
                        }
                        
                        break;
                }
            } else {
                animStack[anim][id].play();
            }
        }

        function stopAnimaton(id, anim) {
            if (id === -1) {
                switch (anim) {
                    case ANIM_ACTIVE :
                        switch (CURRENT_PLAYER) {
                            case 0 :
                                id = "#left";
                                break;
                            case 1 :
                                id = "#right";
                                break;
                        }
                        break;
                }
            }
            switch (anim) {
                case ANIM_ACTIVE :
                    if (animStack[anim][id] !== undefined) {
                        animStack[anim][id].stop();
                    }
                    break;
            }
        }

        $("#btAB1").click(
                function (event) {
                    event.preventDefault();
                    if (useAbility(playerAbilities[CURRENT_PLAYER][0])) {
                        randomSpecialAbility(CURRENT_PLAYER, 0);
                        switchPlayer();
                    }
                }
        );
        $("#btAB2").click(
                function (event) {
                    event.preventDefault();
                    if (useAbility(playerAbilities[CURRENT_PLAYER][1])) {
                        randomSpecialAbility(CURRENT_PLAYER, 1);
                        switchPlayer();
                    }
                }
        );
        $("#btAB3").click(
                function (event) {
                    event.preventDefault();
                    if (useAbility(playerAbilities[CURRENT_PLAYER][2])) {
                        randomSpecialAbility(CURRENT_PLAYER, 2);
                        switchPlayer();
                    }
                }
        );

        $("#btMagic").click(
                function (event) {
                    event.preventDefault();
                    switch (godsNames[CURRENT_PLAYER]) {
                        case ARES :
                            useAbility(DAMAGE);
                            break;
                        case LOKI :
                            useAbility(EVADE);
                            break;
                        case XIPE :
                            useAbility(HEAL);
                            break;
                    }
                    switchPlayer();
                }
        );

        $("#btAttack").click(
                function (event) {
                    event.preventDefault();
                    attack(1);
                    switchPlayer();
                }
        );
        $("#btDefence").click(
                function (event) {
                    event.preventDefault();
                    useAbility(DEFENCE);
                    switchPlayer();
                }
        );
        $("#newGame").click(
                function (event) {
                    event.preventDefault();
                    newGame();
                }
        );
        $("#closeGame").click(
                function (event) {
                    event.preventDefault();
                    window.close();
                    $("#sceneFight").css("display", "none");
                    $("#startScreen").css("display", "block");
                }
        );
        $(document).ready(function () {
            newGame();
        });
    });
})(jQuery);